package br.edu.fatec.lucasdutra.calculojuroscomposto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;

import br.edu.fatec.lucasdutra.calculojuroscomposto.calculo.CalculoDTO;
import br.edu.fatec.lucasdutra.calculojuroscomposto.calculo.Juros;
import br.edu.fatec.lucasdutra.calculojuroscomposto.calculo.RetornoCalculoDTO;

public class MainActivity extends AppCompatActivity {
    public EditText txtCapital;
    public EditText txtMeses;
    public EditText txtTaxa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCapital = (EditText)findViewById(R.id.txtCapitalInvestido);
        txtMeses = (EditText)findViewById(R.id.txtPeriodo);
        txtTaxa = (EditText)findViewById(R.id.txtPercentualJuros);
    }

    public void Calcular(View view)
    {
        CalculoDTO _calculoDTO = new CalculoDTO();

        _calculoDTO.setQuantidadeMeses(Integer.parseInt(txtMeses.getText().toString()));
        _calculoDTO.setTaxaJurosMensal(Double.parseDouble(txtTaxa.getText().toString()));
        _calculoDTO.setValorInvestido(Double.parseDouble(txtCapital.getText().toString()));

        Juros juros = new Juros(_calculoDTO);

        RetornoCalculoDTO retornoCalculoDTO = new RetornoCalculoDTO();

        retornoCalculoDTO = juros.Calcular();


        Intent it = new Intent(this, Calculo.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("retorno", retornoCalculoDTO);
        it.putExtras(bundle);

        startActivity(it);
    }
}