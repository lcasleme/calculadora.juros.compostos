package br.edu.fatec.lucasdutra.calculojuroscomposto.calculo;

import java.io.Serializable;

public class RetornoCalculoDTO  implements Serializable {
    private String MontanteFinal;
    private String ValorInvestido;
    private String TaxaJurosMensal;
    private String QuantidadeMeses;

    public String getQuantidadeMeses() { return QuantidadeMeses; }

    public String getTaxaJurosMensal() { return TaxaJurosMensal; }

    public String getValorInvestido() { return ValorInvestido; }

    public String getMontanteFinal() { return MontanteFinal; }

    public void setQuantidadeMeses(Integer quantidadeMeses) {

        QuantidadeMeses = "PERÍODO: " + quantidadeMeses.toString() + " meses";
    }

    public void setTaxaJurosMensal(Double taxaJurosMensal) {
        TaxaJurosMensal = "TAXA DE JUROS: " + taxaJurosMensal.toString() + "% ao mês";
    }

    public void setValorInvestido(Double valorInvestido) {
        ValorInvestido = "CAPITAL INVESTIDO: R$ " + valorInvestido.toString();
    }

    public void setMontanteFinal(Double montanteFinal) {
        MontanteFinal = "MONTANTE FINAL: R$ " + montanteFinal.toString();
    }
}
