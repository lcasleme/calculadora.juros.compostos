package br.edu.fatec.lucasdutra.calculojuroscomposto.calculo;

import java.math.BigDecimal;

public class Juros {
    private CalculoDTO _calculoDTO;

    public Juros(CalculoDTO calculoDTO)
    {
        _calculoDTO = calculoDTO;
    }

    public RetornoCalculoDTO Calcular(){
        RetornoCalculoDTO retornoCalculoDTO = new RetornoCalculoDTO();

        retornoCalculoDTO.setValorInvestido(_calculoDTO.getValorInvestido());
        retornoCalculoDTO.setQuantidadeMeses(_calculoDTO.getQuantidadeMeses());
        retornoCalculoDTO.setTaxaJurosMensal(_calculoDTO.getTaxaJurosMensal());

        Double montante = _calculoDTO.getValorInvestido() * (Math.pow((1 + _calculoDTO.getTaxaJurosMensal()) ,_calculoDTO.getQuantidadeMeses()));

        retornoCalculoDTO.setMontanteFinal(montante);

        return  retornoCalculoDTO;
    }
}
