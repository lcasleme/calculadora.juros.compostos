package br.edu.fatec.lucasdutra.calculojuroscomposto.calculo;

public class CalculoDTO {
    private Double ValorInvestido = 0.0;
    private Double TaxaJurosMensal = 0.0;
    private Integer QuantidadeMeses = 0;

    public Integer getQuantidadeMeses() { return QuantidadeMeses; }

    public Double getTaxaJurosMensal() { return TaxaJurosMensal; }

    public Double getValorInvestido() { return ValorInvestido; }

    public void setQuantidadeMeses(Integer quantidadeMeses) {
        QuantidadeMeses = quantidadeMeses;
    }

    public void setTaxaJurosMensal(Double taxaJurosMensal) { TaxaJurosMensal = taxaJurosMensal; }

    public void setValorInvestido(Double valorInvestido) { ValorInvestido = valorInvestido; }
}
