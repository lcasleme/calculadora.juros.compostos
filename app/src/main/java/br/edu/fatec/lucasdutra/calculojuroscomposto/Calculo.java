package br.edu.fatec.lucasdutra.calculojuroscomposto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import br.edu.fatec.lucasdutra.calculojuroscomposto.calculo.RetornoCalculoDTO;

public class Calculo extends AppCompatActivity {
    public TextView lblCapital;
    public TextView lblTaxa;
    public TextView lblMeses;
    public TextView lblMontante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculo);

        lblCapital = (TextView)findViewById(R.id.lblCapital);
        lblTaxa = (TextView)findViewById(R.id.lblTaxa);
        lblMeses = (TextView)findViewById(R.id.lblMeses);
        lblMontante = (TextView)findViewById(R.id.lblMontante);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

        RetornoCalculoDTO retornoCalculoDTO  = (RetornoCalculoDTO)bundle.getSerializable("retorno");

        lblCapital.setText(retornoCalculoDTO.getValorInvestido());
        lblTaxa.setText(retornoCalculoDTO.getTaxaJurosMensal());
        lblMeses.setText(retornoCalculoDTO.getQuantidadeMeses());
        lblMontante.setText(retornoCalculoDTO.getMontanteFinal());
    }
}